<?php
/**
 * Plugin Name:			kreatif addon & functionality
 * Description:			Create functionality and modify external plugin.
 * Version:				1.0.0
 * Author:				M. Aris Setiaawan
 * Author URI:			https://madebyaris.net
 * Requires at least:	4.5.0
 *
 * Text Domain: kreatif-addon
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

$path = plugin_dir_path( __FILE__ );

if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {
    require_once $path . 'gears/woocommerce.php';
}
