<?php
/**
 * Woocommerce
 *
 * This is where we modify and add functionality woocommerce.
 *
 */

 // If accessed directly, abort!
if ( ! defined( 'ABSPATH' ) ) {
 exit( 'You shall not pass!' );
}

class ModifyWoocommerce {
  var $prefix = 'kreatif-addon';

  public function __construct() {
    add_filter( 'woocommerce_default_address_fields', array( $this, 'modify_woo_address_fields' ), 99 );
    add_filter( 'woocommerce_checkout_fields', array( $this, 'modify_woo_checkout_fields') );
    add_filter( 'woocommerce_default_address_fields', array( $this, 'modify_woo_address_fields') );
    add_filter( 'woocommerce_order_button_text', array( $this, 'change_place_order_text' ), 1);

    add_action( 'wp_enqueue_scripts', array( $this, 'scripts_add_woo_select') );
    add_action( 'wp_enqueue_scripts', array( $this, 'script_product_single') );
    add_action( 'after_setup_theme', array( $this, 'disable_zoom_single'), 90 );
  }

  /*
   * if you want to modify some address fields, you modify in here, this is for some special case
   *
   * @url https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
   */
  public function modify_woo_address_fields( $fields){

    $fields[ 'address_1' ][ 'priority' ] = 20;
    $fields[ 'address_1' ][ 'label' ] = _('Alamat Lengkap');

    //$fields[ 'billing_city' ][ 'priority' ] = 10;

    return $fields;
  }
  public function modify_woo_checkout_fields( $fields ) {
    $prefix = 'kreatif-addon';

    // Change the label
    $fields['billing']['billing_first_name']['label'] = 'Nama Lengkap';
  	$fields['billing']['billing_phone']['label'] = 'Nomer HP/whatsapp Aktif';
  	$fields['billing']['billing_email']['label'] = 'Email';
    $fields['order']['order_comments']['label'] = 'keterangan';
    $fields['order']['order_comments']['placeholder'] = 'keterangan alamat rumah/ancer-ancer dan lainya';

    // Change the type of input
    $fields['billing']['billing_address_1']['type'] = 'textarea';

    // Make fields optional (unrequired)
    $fields['billing']['billing_email']['required'] = false;

    // Re-Order fields
    $fields['billing']['billing_first_name']['priority'] = 10;
    //$fields['billing']['billing_city']['priority'] = 30;
    $fields['billing']['billing_address_2']['priority'] = 20;

    //$fields['billing']['billing_address_3']['priority'] = 20;
    $fields['billing']['billing_postcode']['priority'] = 60;
    $fields['billing']['billing_phone']['priority'] = 70;
    $fields['billing']['billing_email']['priority'] = 80;

    // Change class css
    $fields['billing']['billing_first_name']['class'] = array();
    $fields['billing']['billing_postcode']['class'] = array();
    $fields['billing']['billing_city']['class'] = array();


    // Remove Uneeded fields
    unset( $fields[ 'billing' ][ 'billing_state' ] );
    unset( $fields[ 'billing' ][ 'billing_last_name' ] );
    unset( $fields[ 'billing' ][ 'billing_company' ] );

  	return $fields;
  }

  public function disable_zoom_single() {
    remove_theme_support( 'wc-product-gallery-zoom' );
  }

  public function scripts_add_woo_select() {

    if ( ! is_page( 'checkout' ) ) return;

    wp_register_script( 'wooSelectScripts_mod',
    plugin_dir_url( __DIR__ ) . '/assets/js/woo-custom.js',
    array( 'select2' ),'1', TRUE );

    wp_register_style( 'WooCheckoutStyles_mod',
    plugin_dir_url( __DIR__ ) . '/assets/css/woocoomerce-billing.css');

    wp_enqueue_style( 'WooCheckoutStyles_mod' );
    wp_enqueue_script( 'wooSelectScripts_mod' );
  }

  public function script_product_single() {

    if ( ! is_product() ) return;

    wp_register_script( 'wooscript_price_variation',
    plugin_dir_url( __DIR__ ) . '/assets/js/woo-price-variation.js',
    array( 'jquery' ),'1', TRUE );
    wp_enqueue_script( 'wooscript_price_variation' );
  }

  public function change_place_order_text( $order_button_text ) {
    return 'Bayar';
  }

}

new ModifyWoocommerce();
